class AddCreatedByForPostsAndComments < ActiveRecord::Migration
  def up
    add_column("posts", "created_by", :string, :limit => 25)
    add_column("comments", "created_by", :string, :limit => 25)
  end

  def down
    remove_column("comments", "created_by")
    remove_column("posts", "created_by")
  end
end
