require 'rails_helper'

describe "posts/index" do
  before(:each) do
    assign(:posts, [
      stub_model(Post, title: "Title", body: "MyText"),
      stub_model(Post, title: "Title", body: "MyText")
    ])
  end

  it "renders a list of posts" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", text: "Title", count: 2
    assert_select "tr>td", text: "MyText", count: 2
  end
end
